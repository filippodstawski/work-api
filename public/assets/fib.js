$( document ).ready(function() {

    $( "#result-box" ).hide();

    $( "#count" ).click(function() {
        var number = $( "#number" ).val();
        $.ajax({
            type: "POST",
            data: {"number" : number},
            url: "/api",
            success: function(data){
                $( "#result-box" ).show();
                $( "#result-number" ).html(data.result);
            }
        });
    });

});