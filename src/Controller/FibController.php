<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class FibController extends AbstractController
{
    public function index()
    {
        return $this->render('form.html.twig', []);
    }
}