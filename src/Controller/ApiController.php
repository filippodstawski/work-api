<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Service\FibService;

class ApiController extends AbstractController
{

    private $fibService;

    public function __construct(FibService $fibService)
    {
        $this->fibService = $fibService;
    }

    public function result()
    {

        $request = Request::createFromGlobals();
        $number = $request->request->get('number');

        $response = new Response();
        $response->setContent(json_encode([
            'result' => $this->fibService->highestNumberInN($number),
        ]));
        $response->headers->set('Content-Type', 'application/json');

        return $response;

    }
}