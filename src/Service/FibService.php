<?php

namespace App\Service;

class FibService
{
    public function highestNumberInN(int $number): int{
        if ($number == 0) 
            return 0;     
        else if ($number == 1) 
            return 1;     
        else
            return ($this->highestNumberInN($number-1) +  
                    $this->highestNumberInN($number-2)); 
    }
}