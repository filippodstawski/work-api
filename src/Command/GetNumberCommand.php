<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use App\Service\FibService;

class GetNumberCommand extends Command
{

    private $fibService;

    public function __construct(FibService $fibService)
    {
        $this->fibService = $fibService;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('app:get-number')
            ->setDescription('Get number command')
            ->addArgument('number', InputArgument::REQUIRED, 'Number to check.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'number is: ' . $this->fibService->highestNumberInN($input->getArgument('number'))
        ]);
        return 0;
    }
}